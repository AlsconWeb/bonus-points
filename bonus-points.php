<?php
/**
 * Bonus Points.
 *
 * @author            Alex L
 * @license           GPL-2.0-or-later
 * @wordpress-plugin
 *
 * Plugin Name:       Bonus Points
 * Plugin URI:        https://i-wp-dev.com/
 * Description:       Our loyalty points plugin awards bonus points to customers for each purchase they make on your
 *                    website. These points can then be redeemed for discounts or other rewards, incentivizing
 *                    customers to return and make additional purchases. With our plugin, you can easily set up a
 *                    points system that encourages repeat business and boosts customer loyalty.
 * Version:           1.2.5
 * Requires at least: 5.6.0
 * Requires PHP:      7.4
 * Author:            Alex L
 * Author URI:        https://i-wp-dev.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       bp
 * Domain Path:       /languages/
 *
 * @package           bp/wordpress-plugin
 */

/**
 * Plugin version.
 */

use BP\Plugin\Admin\Notification;
use BP\Plugin\Main;

const BP_VERSION = '1.2.5';

/**
 * Path to the plugin dir.
 */
const BP_PATH = __DIR__;

/**
 * Plugin dir url.
 */
define( 'BP_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

/**
 * Minimum required php version.
 */
const BP_MINIMUM_PHP_REQUIRED_VERSION = '7.4';

/**
 * Main plugin file.
 */
const BP_FILE = __FILE__;

/**
 * Autoload class.
 */
require_once BP_PATH . '/vendor/autoload.php';

/**
 * Check php version.
 */
if ( ! Main::is_php_version() ) {

	add_action( 'admin_notices', [ Notification::class, 'php_version_nope' ] );

	if ( is_plugin_active( plugin_basename( BP_FILE ) ) ) {
		deactivate_plugins( plugin_basename( BP_FILE ) );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

	return;
}

/**
 * Check active WooCommerce.
 */
require_once ABSPATH . 'wp-admin/includes/plugin.php';

if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
	add_action( 'admin_notices', [ Notification::class, 'woocommerce_active' ] );
	deactivate_plugins( plugin_basename( BP_FILE ) );
	if ( isset( $_GET['activate'] ) ) {
		unset( $_GET['activate'] );
	}
}

new Main();

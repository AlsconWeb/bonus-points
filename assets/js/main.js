jQuery( document ).ready( function( $ ) {
	writeOffBonuses();
} );

function writeOffBonuses() {
	let bonusEL = $( '#redeem-bonuses' );
	console.log(bonusEL);
	if ( bonusEL.length ) {
		bonusEL.change( function( e ) {
			let val = $( this ).val();
			let maxVal = $( this ).attr( 'max' );
			let updateCartBt = $( '[name=update_cart]' );

			let totalVal = val;

			if ( val > maxVal ) {
				totalVal = maxVal;
			}

			bonusEL.val( totalVal );

			console.log( val, totalVal );

			setCookie( 'bp_fee', totalVal, 1 );

			updateCartBt.attr( 'disabled', false );
		} );
	}
}

jQuery( document.body ).on( 'updated_wc_div', function() {
	writeOffBonuses();
} );


/**
 * Set Cookie.
 *
 * @param name Cookie name.
 * @param value Cookie value.
 * @param days Cookie date.
 */
function setCookie( name, value, days ) {
	let expires = '';
	if ( days ) {
		var date = new Date();
		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
		expires = '; expires=' + date.toUTCString();
	}
	document.cookie = name + '=' + ( value || '' ) + expires + '; path=/';
}

/**
 * Get Cookie.
 *
 * @param name Cookie name.
 * @returns {null|string}
 */
function getCookie( name ) {
	let nameEQ = name + '=';
	let ca = document.cookie.split( ';' );

	for ( let i = 0; i < ca.length; i++ ) {
		let c = ca[ i ];
		while ( c.charAt( 0 ) === ' ' ) c = c.substring( 1, c.length );
		if ( c.indexOf( nameEQ ) === 0 ) return c.substring( nameEQ.length, c.length );
	}

	return null;
}

/**
 * Delete cookie.
 *
 * @param name
 */
function deleteCookie( name ) {
	document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
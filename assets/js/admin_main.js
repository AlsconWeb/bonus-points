/* global  BpScriptObject */

/**
 * @param BpScriptObject.ajaxUrl
 * @param BpScriptObject.actionName
 * @param BpScriptObject.nonceSync
 */
jQuery( document ).ready( function( $ ) {

	let syncButtonEl = $( '.button-sync button' );
	console.log( syncButtonEl );

	if ( syncButtonEl.length ) {

		syncButtonEl.click( function( e ) {
			e.preventDefault();

			let data = {
				action: BpScriptObject.actionName,
				nonceSync: BpScriptObject.nonceSync
			};

			$.ajax( {
				type: 'POST',
				url: BpScriptObject.ajaxUrl,
				data: data,
				beforeSend: function() {
					$( '.spinner.sync' ).css( {
						visibility: 'visible',
					} );
				},
				success: function( res ) {
					if ( res.success ) {
						$( '.spinner.sync' ).css( {
							visibility: 'hidden',
						} );
					} else {
						alert( res.data.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

} );
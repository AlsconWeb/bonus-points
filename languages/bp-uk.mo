��          �      |      �     �  '        *     3     B     X     j  !   |     �  ^   �          1  ;   @  I   |     �     �  
   �  #   �               )  �  6     �  C        K     c  4        �  &   �  >   �  /   8  �   h  :   4  +   o  g   �  ~   	  
   �	     �	  !   �	  F   �	     
  -   
     F
                                                                                	   
               Your password:  Add products with a discount to bonuses All done Bad nonce code Bonus Points Settings Bonuses available Discount on order For how much one point is charged Login details on site:  Our loyalty points plugin awards bonus points to customers for each purchase they make on your Poster personal Integration Redeem bonuses Select the ID of the group to which new users will be added Specify the available percentage of the order amount to write off bonuses Start Synchronization User bonus Welcome to our website! Your login: Yes You can write off bonuses Your bonuses Project-Id-Version: Bonus Points 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/bonus-points
PO-Revision-Date: 2023-04-19 11:21+0200
Last-Translator: 
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Domain: bp
 Ваш пароль: Додайте до бонусів товари зі знижкою Все зроблено Поганий код nonce Налаштування бонусних балів Доступні бонуси Знижка на замовлення За скільки нараховується один бал Деталі для входу на сайті: Наш плагін балів лояльності нараховує клієнтам бонусні бали за кожну покупку, яку вони роблять на вашому сайті Плакат персональної інтеграції Використовувати бонуси Виберіть ID групи, до якої буде додано нових користувачів Вкажіть доступний відсоток від суми замовлення для списання бонусів Старт Синхронізація Бонус користувача Ласкаво просимо на наш сайт! Ваш логін: Так Ви можете списати бонуси Ваші бонуси 
# Copyright (C) 2023 Alex L
# This file is distributed under the same license as the Bonus Points plugin.
msgid ""
msgstr ""
"Project-Id-Version: Bonus Points 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/bonus-points\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2023-04-19T09:14:06+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: bp\n"

#. Plugin Name of the plugin
msgid "Bonus Points"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "https://i-wp-dev.com/"
msgstr ""

#. Description of the plugin
msgid "Our loyalty points plugin awards bonus points to customers for each purchase they make on your"
msgstr ""

#. Author of the plugin
msgid "Alex L"
msgstr ""

#: src/php/Api/Poster/PosterApi.php:72
msgid "Bad nonce code"
msgstr ""

#: src/php/Api/Poster/PosterApi.php:109
msgid "All done"
msgstr ""

#: src/php/Api/Poster/PosterApi.php:337
msgid "Login details on site: "
msgstr ""

#: src/php/Api/Poster/PosterApi.php:338
msgid "Welcome to our website! Your login:"
msgstr ""

#: src/php/Api/Poster/PosterApi.php:338
msgid " Your password: "
msgstr ""

#: src/php/CarbonFields/PluginOption.php:56
msgid "Bonus Points Settings"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:61
msgid "Add products with a discount to bonuses"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:65
msgid "Yes"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:68
msgid "For how much one point is charged"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:70
msgid "Specify the available percentage of the order amount to write off bonuses"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:73
msgid "Poster personal Integration"
msgstr ""

#: src/php/CarbonFields/PluginOption.php:75
msgid "Select the ID of the group to which new users will be added"
msgstr ""

#: src/php/Helpers.php:24
msgid "Synchronization"
msgstr ""

#: src/php/Helpers.php:27
msgid "Start"
msgstr ""

#: src/php/Main.php:164
msgid "User bonus"
msgstr ""

#: src/php/WooCommerce/BonusSystems.php:369
msgid "Discount on order"
msgstr ""

#: src/php/WooCommerce/BonusSystems.php:427
#: src/php/WooCommerce/BonusSystems.php:448
msgid "Your bonuses"
msgstr ""

#: src/php/WooCommerce/BonusSystems.php:457
#: template/cart.php:182
#: template/myaccount/orders.php:126
msgid "Bonuses available"
msgstr ""

#: template/cart.php:188
msgid "You can write off bonuses"
msgstr ""

#: template/cart.php:194
msgid "Redeem bonuses"
msgstr ""

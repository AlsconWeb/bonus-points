��          �      |      �     �  '        *     3     B     X     j  !   |     �  ^   �          1  ;   @  I   |     �     �  
   �  #   �               )  �  6     �  E        M  *   c  0   �     �     �  <   �  -   9  �   g  .   8  %   g  y   �  t   	  
   |	     �	  +   �	  O   �	     
  -   #
     Q
                                                                                	   
               Your password:  Add products with a discount to bonuses All done Bad nonce code Bonus Points Settings Bonuses available Discount on order For how much one point is charged Login details on site:  Our loyalty points plugin awards bonus points to customers for each purchase they make on your Poster personal Integration Redeem bonuses Select the ID of the group to which new users will be added Specify the available percentage of the order amount to write off bonuses Start Synchronization User bonus Welcome to our website! Your login: Yes You can write off bonuses Your bonuses Project-Id-Version: Bonus Points 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/bonus-points
PO-Revision-Date: 2023-04-19 11:23+0200
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Domain: bp
 Ваш пароль: Добавляйте товары со скидкой в бонусы Все сделано Плохой одноразовый код Настройки бонусных баллов Доступные бонусы Скидка на заказ За сколько начисляется один балл Данные для входа на сайт: Наш плагин баллов лояльности начисляет бонусные баллы клиентам за каждую покупку, которую они совершают на вашем Плакат личной интеграции Использовать бонусы Выберите ID группы, в которую будут добавляться новые пользователи Укажите доступный процент от суммы заказа для списания бонусов Старт Синхронизация Пользовательский бонус Добро пожаловать на наш веб-сайт! Ваш логин: Да Вы можете списать бонусы Ваши бонусы 
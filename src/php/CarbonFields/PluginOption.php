<?php
/**
 * Init Plugin Options page.
 *
 * @package bp/wordpress-plugin
 */

namespace BP\Plugin\CarbonFields;

use BP\Plugin\Api\Poster\PosterApi;
use BP\Plugin\Helpers;
use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * PluginOption class file.
 */
class PluginOption {
	/**
	 * PluginOption construct.
	 */
	public function __construct() {

		$this->init();
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_load' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_plugin_options_page' ] );
	}

	/**
	 * Load carbon fields boot().
	 *
	 * @return void
	 */
	public function carbon_fields_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add plugin option page field.
	 *
	 * @return void
	 */
	public function add_plugin_options_page(): void {

		$poster = new PosterApi();

		Container::make( 'theme_options', __( 'Bonus Points Settings', 'bp' ) )
			->set_icon( 'dashicons-money-alt' )
			->set_page_menu_position( 40 )
			->add_fields(
				[
					Field::make( 'radio', 'bp_sales_in_bonus', __( 'Add products with a discount to bonuses', 'bp' ) )
						->set_options(
							[
								'no'  => __( 'No', 'pb' ),
								'yes' => __( 'Yes', 'bp' ),
							]
						),
					Field::make( 'text', 'pb_points_per_currency_unit', __( 'For how much one point is charged', 'bp' ) )
						->set_attribute( 'type', 'number' ),
					Field::make( 'text', 'pb_percentage_of_cart', __( 'Specify the available percentage of the order amount to write off bonuses', 'bp' ) )
						->set_default_value( 10 )
						->set_attribute( 'type', 'number' ),
					Field::make( 'text', 'pb_poster_token', __( 'Poster personal Integration', 'bp' ) )
						->set_attribute( 'type', 'password' ),
					Field::make( 'select', 'bp_create_new_user_in_group', __( 'Select the ID of the group to which new users will be added', 'bp' ) )
						->set_options( $poster->get_poster_groups( true ) ),
					Field::make( 'html', 'pb_information_spinner' )
						->set_html( Helpers::get_html_sync() ),
				]
			);
	}
}

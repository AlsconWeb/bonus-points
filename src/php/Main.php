<?php
/**
 * Bonus Points main class file.
 *
 * @package bp/wordpress-plugin
 */

namespace BP\Plugin;

use BP\Plugin\Api\Poster\PosterApi;
use BP\Plugin\CarbonFields\PluginOption;
use BP\Plugin\WooCommerce\BonusSystems;

/**
 * Main class file.
 */
class Main {

	/**
	 * Table name bonus point.
	 */
	public const BP_TABLE_NAM = 'bonus_point_user';

	/**
	 * Bonus system.
	 *
	 * @var BonusSystems
	 */
	private $bonus_sys;

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		$this->bonus_sys = new BonusSystems();
		new PluginOption();
	}

	/**
	 * Init hooks and action.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'init', [ $this, 'create_user_bonus_table' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'init', [ $this, 'poster_integration' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_scripts' ] );

		add_filter( 'manage_users_columns', [ $this, 'add_bonus_column_in_user' ] );
		add_filter( 'manage_users_custom_column', [ $this, 'show_user_bonus_in_users' ], 10, 3 );

		load_plugin_textdomain( 'bp', false, dirname( plugin_basename( BP_FILE ) ) . '/languages' );
	}

	/**
	 * Check php version.
	 *
	 * @return bool
	 * @noinspection ConstantCanBeUsedInspection
	 */
	public static function is_php_version(): bool {
		if ( version_compare( BP_MINIMUM_PHP_REQUIRED_VERSION, phpversion(), '>' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Create bonus table.
	 *
	 * @return void
	 */
	public function create_user_bonus_table(): void {
		$table_create = get_option( 'bp_table_user', false );

		if ( ! $table_create ) {
			global $wpdb;

			$table_prefix    = $wpdb->base_prefix;
			$charset_collate = $wpdb->get_charset_collate();

			$sql = 'CREATE TABLE IF NOT EXISTS ' . $table_prefix . self::BP_TABLE_NAM . ' 
					(
						`id` BIGINT NOT NULL AUTO_INCREMENT , 
						`user_id` BIGINT NOT NULL UNIQUE, 
						`bonus_count` INT NULL DEFAULT "0" , 
						`poster_id` BIGINT NULL , 
						PRIMARY KEY (`id`)
					) ENGINE = InnoDB ' . $charset_collate . ';';

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';

			dbDelta( $sql );

			add_option( 'bp_table_user', true );
		}

	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		if ( is_cart() && ! empty( WC()->cart ) || is_account_page() ) {
			wp_enqueue_script( 'bp_main', BP_URL . '/assets/js/main' . $min . '.js', [ 'jquery' ], BP_VERSION, true );
			wp_enqueue_style( 'bp_style', BP_URL . '/assets/css/style' . $min . '.css', null, BP_VERSION );
		}

	}

	/**
	 * Poster CRM integration.
	 *
	 * @return void
	 */
	public function poster_integration(): void {
		$poster_token = carbon_get_theme_option( 'pb_poster_token' );

		if ( empty( $poster_token ) ) {
			return;
		}

		$poster = new PosterApi();
	}

	/**
	 * Add style and scripts in admin.
	 *
	 * @return void
	 */
	public function add_admin_scripts(): void {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_script( 'bp_admin_main', BP_URL . '/assets/js/admin_main' . $min . '.js', [ 'jquery' ], BP_VERSION, true );
		wp_enqueue_style( 'bp_admin-style', BP_URL . '/assets/css/admin-style' . $min . '.css', null, BP_VERSION );

		wp_localize_script(
			'bp_admin_main',
			'BpScriptObject',
			[
				'ajaxUrl'    => admin_url( 'admin-ajax.php' ),
				'actionName' => PosterApi::BP_SYNC_ACTION_NAME,
				'nonceSync'  => wp_create_nonce( PosterApi::BP_SYNC_ACTION_NAME ),
			]
		);
	}

	/**
	 * Add bonus columns.
	 *
	 * @param array $columns Columns.
	 *
	 * @return array
	 */
	public function add_bonus_column_in_user( array $columns ): array {
		$columns['bp_bonus'] = __( 'User bonus', 'bp' );

		return $columns;
	}

	/**
	 * Show user bonus.
	 *
	 * @param string $value
	 * @param string $column_name
	 * @param int    $user_id
	 *
	 * @return string
	 */
	public function show_user_bonus_in_users( string $value, string $column_name, int $user_id ): string {
		if ( 'bp_bonus' === $column_name ) {
			$value = $this->bonus_sys->get_user_bonus_count( $user_id );
		}

		return $value;
	}
}

<?php
/**
 * Admin notification class.
 *
 * @package bp/wordpress-plugin
 */

namespace BP\Plugin\Admin;

/**
 * Notification class file.
 */
class Notification {

	/**
	 * Low Version PHP.
	 */
	public static function php_version_nope(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			wp_kses(
				sprintf(
				/* translators: 1: Required PHP version number, 2: Current PHP version number, 3: URL of PHP update help page */
					__( 'The Poster Customer Sync plugin requires PHP version %1$s or higher. This site is running PHP version %2$s. <a href="%3$s">Learn about updating PHP</a>.', 'pcs' ),
					BP_MINIMUM_PHP_REQUIRED_VERSION,
					PHP_VERSION,
					'https://wordpress.org/support/update-php/'
				),
				[
					'a' => [
						'href' => [],
					],
				]
			)
		);
	}

	/**
	 * Activation Woocommerce Plugin.
	 */
	public static function woocommerce_active(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-warning is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'For Bonus points to work, you need to activate the Woocommerce Plugin', 'pcs' ),
			)
		);
	}
}

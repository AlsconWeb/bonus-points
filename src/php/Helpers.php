<?php
/**
 * Helpers class.
 *
 * @package bp/wordpress-plugin
 */

namespace BP\Plugin;

/**
 * Helpers class file.
 */
class Helpers {

	/**
	 * Get html button in options page.
	 *
	 * @return string
	 */
	public static function get_html_sync(): string {
		ob_start();
		?>
		<div>
			<h4 class="label"><?php esc_html_e( 'Synchronization', 'bp' ); ?></h4>
		</div>
		<div class="cf-field__body button-sync">
			<button><?php esc_html_e( 'Start', 'bp' ); ?></button>
		</div>

		<div class="spinner sync"></div>
		<?php
		return ob_get_clean();
	}

	/**
	 * Isset user in bonus system.
	 *
	 * @param int $user_id Postal user ID.
	 *
	 * @return bool
	 */
	public static function isset_user_in_bonus_sys( int $user_id ): bool {
		global $wpdb;
		$prefix     = $wpdb->prefix;
		$table_name = $prefix . Main::BP_TABLE_NAM;

		// phpcs:disable
		$result = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM ' . $table_name . ' WHERE poster_id = %d;', $user_id ) );
		// phpcs:enable
		if ( empty( $result ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Link user by ID.
	 *
	 * @param int $wp_user_id Wp user id.
	 * @param int $poster_id  Poster client id.
	 * @param int $user_bonus User poster bonus.
	 *
	 * @return void
	 */
	public static function link_users_by_id( int $wp_user_id, int $poster_id, int $user_bonus ): void {
		global $wpdb;
		$prefix     = $wpdb->prefix;
		$table_name = $prefix . Main::BP_TABLE_NAM;

		//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		$result = $wpdb->update(
			$table_name,
			[
				'poster_id'   => $poster_id,
				'bonus_count' => $user_bonus,
			],
			[ 'user_id' => $wp_user_id ],
			[ '%d', '%d' ],
			[ '%d' ]
		);

		if ( false === $result ) {
			$wpdb->insert(
				$table_name,
				[
					'user_id'     => $wp_user_id,
					'poster_id'   => $poster_id,
					'bonus_count' => $user_bonus,
				],
				[ '%d', '%d', '%d' ]
			);
		}
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
	}

	/**
	 * Update user bonus.
	 *
	 * @param int $wp_user_id Wp user ID.
	 * @param int $user_bonus Poster user bonus.
	 * @param int $client_id  Poster user id.
	 *
	 * @return void
	 */
	public static function update_user_bonus( int $wp_user_id, int $user_bonus, int $client_id ) {
		global $wpdb;
		$prefix     = $wpdb->prefix;
		$table_name = $prefix . Main::BP_TABLE_NAM;

		$result = $wpdb->update(
			$table_name,
			[
				'bonus_count' => $user_bonus,
				'poster_id'   => $client_id,
			],
			[
				'user_id' => $wp_user_id,
			],
			[ '%d', '%d' ],
			[ '%d' ]
		);

		if ( ! $result ) {
			$result = $wpdb->insert(
				$table_name,
				[
					'user_id'     => $wp_user_id,
					'bonus_count' => $user_bonus,
					'poster_id'   => $client_id,
				]
			);
		}
	}

	/**
	 * Get all users.
	 *
	 * @return object
	 */
	public static function get_all_users(): object {
		global $wpdb;
		$prefix     = $wpdb->prefix;
		$table_name = $prefix . Main::BP_TABLE_NAM;

		// phpcs:disable
		$result = $wpdb->get_results( 'SELECT * FROM ' . $table_name . ' WHERE poster_id IS NOT NULL ORDER BY user_id;', OBJECT );
		// phpcs:enable
		if ( empty( $result ) ) {
			return (object) [];
		}

		return (object) $result;
	}

	public static function get_poster_id_user( int $wp_user_id ) {
		global $wpdb;
		$prefix     = $wpdb->prefix;
		$table_name = $prefix . Main::BP_TABLE_NAM;

		$result = $wpdb->get_row( 'SELECT poster_id FROM ' . $table_name . ' WHERE user_id = ' . $wp_user_id . ';' );

		if ( ! empty( $result ) ) {
			return $result;
		}

		return (object) [];
	}
}

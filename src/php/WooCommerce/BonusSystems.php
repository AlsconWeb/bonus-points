<?php
/**
 * Main class file bonus systems.
 *
 * @package bp/wordpress-plugin
 */

namespace BP\Plugin\WooCommerce;

use BP\Plugin\Api\Poster\PosterApi;
use BP\Plugin\Helpers;
use BP\Plugin\Main;
use WC_Order;

/**
 * BonusSystems class file.
 */
class BonusSystems {

	private const BP_LK_PAGE_SLUG = 'bonus-point';

	/**
	 * BonusSystems construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'woocommerce_order_status_completed', [ $this, 'add_bonus_user' ] );
		add_action( 'user_register', [ $this, 'add_new_user_in_bonus_system' ] );
		add_action( 'woocommerce_cart_calculate_fees', [ $this, 'add_fee_bonus' ], 20 );
		add_action( 'woocommerce_checkout_order_processed', [ $this, 'subtraction_of_bonuses' ], 10, 3 );
		add_action( 'woocommerce_created_customer', [ $this, 'add_new_user_in_poster' ], 40, 3 );
		add_action( 'woocommerce_account_' . self::BP_LK_PAGE_SLUG . '_endpoint', [ $this, 'show_bonus_page' ] );
		add_action( 'init', [ $this, 'add_custom_account_endpoint' ] );

		add_filter( 'woocommerce_locate_template', [ $this, 'change_template_bonus' ], 10, 3 );
		add_filter( 'woocommerce_account_menu_items', [ $this, 'add_custom_account_menu_item' ], 10, 1 );
		add_filter( 'woocommerce_account_menu_items', [ $this, 'change_order_in_my_account' ], 20 );
	}

	/**
	 * Add bonus from order.
	 *
	 * @param int $order_id WooCommerce order id.
	 *
	 * @return void
	 */
	public function add_bonus_user( int $order_id ): void {
		$order = wc_get_order( $order_id );

		if ( empty( $order ) ) {
			return;
		}

		$order_data         = $order->get_data();
		$customer_id        = (int) $order_data['customer_id'];
		$sales_on           = carbon_get_theme_option( 'bp_sales_in_bonus' );
		$total_bonus        = 0;
		$current_user_bonus = 0;

		if ( ! $this->is_isset_user( $customer_id ) ) {
			$this->add_new_user_in_bonus_system( $customer_id );
		} else {
			$current_user_bonus = $this->get_user_bonus_count( $customer_id );
		}

		if ( 'yes' === $sales_on ) {
			$fees       = $order->get_fees();
			$fee_amount = 0;

			foreach ( $fees as $fee ) {
				$fee_amount = abs( $fee->get_total() );
				break;
			}

			$total_bonus = $this->calc_bonus( $order->get_subtotal() - $fee_amount );
		} else {
			$total_bonus = $this->calc_bonus( $this->get_total_no_sells( $order ) );
		}

		$this->set_user_bonus( $total_bonus + $current_user_bonus, $customer_id );
	}

	/**
	 * Check isset user in bonus table.
	 *
	 * @param int $user_id User ID.
	 *
	 * @return bool
	 */
	private function is_isset_user( int $user_id ): bool {

		global $wpdb;

		$table_prefix = $wpdb->base_prefix;
		$table_name   = $table_prefix . Main::BP_TABLE_NAM;

		//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		//phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT user_id FROM  $table_name WHERE user_id = %d",
				$user_id
			),
		);
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching

		if ( empty( $result ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Add new user in bonus system.
	 *
	 * @param int $user_id User ID.
	 *
	 * @return void
	 */
	public function add_new_user_in_bonus_system( int $user_id ): void {

		global $wpdb;

		$table_prefix = $wpdb->base_prefix;
		$table_name   = $table_prefix . Main::BP_TABLE_NAM;

		//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		$wpdb->insert(
			$table_name,
			[
				'user_id'     => $user_id,
				'bonus_count' => 0,
			],
			[
				'%d',
				'%d',
			]
		);
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
	}

	/**
	 * Get user bonus.
	 *
	 * @param int $user_id User ID.
	 *
	 * @return int
	 */
	public function get_user_bonus_count( int $user_id ): int {

		if ( ! $user_id ) {
			return 0;
		}

		global $wpdb;

		$table_prefix = $wpdb->base_prefix;
		$table_name   = $table_prefix . Main::BP_TABLE_NAM;

		//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		//phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT bonus_count FROM  $table_name WHERE user_id = %d",
				$user_id
			),
		);
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		//phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		//phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching

		if ( empty( $result ) ) {
			return 0;
		}

		return (int) $result[0]->bonus_count;
	}

	/**
	 * Calculation bonus.
	 *
	 * @param float $total Total order item sum.
	 *
	 * @return int
	 */
	private function calc_bonus( float $total ): int {
		$points_per_currency = carbon_get_theme_option( 'pb_points_per_currency_unit' ) ?? 20;

		if ( 0 === (int) $total ) {
			return 0;
		}

		return (int) $total / (int) $points_per_currency;
	}

	/**
	 * Get total not sales.
	 *
	 * @param WC_Order $order WooCommerce order.
	 *
	 * @return int
	 */
	private function get_total_no_sells( WC_Order $order ): int {
		$price      = 0;
		$fees       = $order->get_fees();
		$fee_amount = 0;

		foreach ( $fees as $fee ) {
			$fee_amount = abs( $fee->get_total() );
			break;
		}

		foreach ( $order->get_items() as $item ) {
			$product = $item->get_product();

			if ( ! $product->is_on_sale() ) {
				$price += $order->get_item_total( $item );
			}
		}

		return (int) $price - $fee_amount;
	}

	/**
	 * Set user bonus.
	 *
	 * @param int $total_bonus Total set bonus.
	 * @param int $user_id     User ID.
	 * @param int $poster_id   Poster user id.
	 *
	 * @return void
	 */
	public function set_user_bonus( int $total_bonus, int $user_id, $poster_id = null ): void {
		global $wpdb;

		$table_prefix = $wpdb->base_prefix;
		$table_name   = $table_prefix . Main::BP_TABLE_NAM;

		if ( ! empty( $poster_id ) ) {
			//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
			//phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
			$wpdb->update(
				$table_name,
				[
					'bonus_count' => $total_bonus,
					'poster_id'   => $poster_id,
				],
				[ 'user_id' => $user_id ],
				[ '%d', '%d' ],
				[ '%d' ]
			);
			//phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
			//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		} else {
			//phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
			//phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
			$wpdb->update(
				$table_name,
				[
					'bonus_count' => $total_bonus,
				],
				[ 'user_id' => $user_id ],
				[ '%d' ],
				[ '%d' ]
			);
			//phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
			//phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		}
	}

	/**
	 * Add new user in poster.
	 *
	 * @param int   $customer_id        User id.
	 * @param array $new_customer_data  Customer data.
	 * @param bool  $password_generated Password generated.
	 *
	 * @return void
	 */
	public function add_new_user_in_poster( int $customer_id, array $new_customer_data, bool $password_generated ): void {
		if ( ! empty( get_option( '_pb_poster_token', false ) ) ) {
			$poster    = new PosterApi();
			$user_data = get_userdata( $customer_id );

			$new_client['form_params'] = [
				'client_name'             => filter_var( $user_data->first_name . ' ' . $user_data->last_name, FILTER_SANITIZE_STRING ),
				'client_sex'              => 2,
				'client_groups_id_client' => get_option( '_bp_create_new_user_in_group', 0 ),
				'card_number'             => '',
				'discount_per'            => 0,
				'bonus'                   => 0,
				'email'                   => $new_customer_data['user_email'],
			];

			$poster->create_user_in_poster( $new_client );
		}
	}

	/**
	 * Change template path.
	 *
	 * @param string $template      Template path.
	 * @param string $template_name Template name.
	 * @param string $template_path Template dir path.
	 *
	 * @return mixed|string
	 */
	public function change_template_bonus( string $template, string $template_name, string $template_path ) {

		if ( 'cart/cart.php' === $template_name ) {
			$template = BP_PATH . '/template/cart.php';
		}

		if ( 'myaccount/orders.php' === $template_name ) {
			$template = BP_PATH . '/template/myaccount/orders.php';
		}

		return $template;
	}

	/**
	 * Add fee bonus.
	 *
	 * @return void
	 */
	public function add_fee_bonus(): void {

		global $woocommerce;

		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			return;
		}

		$bonus_count  = ! empty( $_POST['redeem_bonuses'] ) ? filter_var( wp_unslash( $_POST['redeem_bonuses'] ), FILTER_SANITIZE_NUMBER_INT ) : null;
		$bonus_cookie = ! empty( $_COOKIE['bp_fee'] ) ? filter_var( wp_unslash( $_COOKIE['bp_fee'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		$current_user_id = get_current_user_id();
		$available_bonus = $this->get_user_bonus_count( $current_user_id );
		$max_bonus       = $this->calculate_available_bonus( $available_bonus );

		if ( empty( $bonus_count ) && empty( $bonus_cookie ) ) {
			return;
		}

		if ( empty( $bonus_count ) && ! empty( $bonus_cookie ) ) {
			$bonus_count = (int) $bonus_cookie;
		}


		if ( $max_bonus >= $bonus_count && empty( $woocommerce->cart->get_coupon_discount_totals() ) ) {
			$woocommerce->cart->add_fee( __( 'Discount on order', 'bp' ), - $bonus_count );
		}
	}

	/**
	 * Calculate available bonus.
	 *
	 * @param int $available_bonus Available bonus.
	 *
	 * @return int
	 */
	public function calculate_available_bonus( int $available_bonus ): int {
		$cart       = WC()->cart;
		$cart_items = $cart->get_cart_contents();
		$total      = 0;

		$available_percent = carbon_get_theme_option( 'pb_percentage_of_cart' ) ?? 20;

		foreach ( $cart_items as $cart_item_key => $cart_item ) {
			if ( ! $cart_item['data']->is_on_sale() ) {
				$total += $cart_item['line_total'];
			}
		}

		$result = $total * ( $available_percent / 100 );

		if ( $result > $available_bonus ) {
			return $available_bonus;
		}

		return (int) $result;
	}

	/**
	 * Deducts from the user's bonus account.
	 *
	 * @param int      $order_id    Order ID.
	 * @param array    $posted_data Order data.
	 * @param WC_Order $order       WOO Order.
	 *
	 * @return void
	 */
	public function subtraction_of_bonuses( int $order_id, array $posted_data, WC_Order $order ) {
		$fees       = $order->get_fees();
		$fee_amount = 0;

		foreach ( $fees as $fee ) {
			$fee_amount = abs( $fee->get_total() );
			break;
		}

		if ( empty( $fee_amount ) ) {
			return;
		}

		$customer_id = $order->get_customer_id();

		$this->update_subtraction_of_bonuses( (int) $fee_amount, $customer_id );
	}

	/**
	 * Update the number of bonuses.
	 *
	 * @param int $fee_amount  Total amount of write-off bonuses.
	 * @param int $customer_id Customer ID.
	 *
	 * @return void
	 */
	private function update_subtraction_of_bonuses( int $fee_amount, int $customer_id ) {
		$customer_total_bonus = $this->get_user_bonus_count( $customer_id );

		$customer_total_bonus -= $fee_amount;

		$this->set_user_bonus( $customer_total_bonus, $customer_id );

		$poster_id = Helpers::get_poster_id_user( $customer_id )->poster_id;

		if ( ! empty( $poster_id ) ) {
			$poster = new PosterApi();
			$poster->set_bonus_in_poster( $poster_id, $customer_total_bonus );
		}

		unset( $_COOKIE['bp_fee'] );
		setcookie( 'bp_fee', 0, time() - 2000 );
	}

	/**
	 * Add bonus page in LK.
	 *
	 * @param array $items Menu items.
	 *
	 * @return array
	 */
	public function add_custom_account_menu_item( array $items ): array {

		$items['bonus-point'] = __( 'Your bonuses', 'bp' );

		return $items;
	}

	/**
	 * Rewrite account endpoint.
	 *
	 * @return void
	 */
	public function add_custom_account_endpoint(): void {
		add_rewrite_endpoint( 'bonus-point', EP_PAGES );
	}

	/**
	 * Show bonus page.
	 *
	 * @return void
	 */
	public function show_bonus_page(): void {
		?>
		<h3><?php esc_html_e( 'Your bonuses', 'bp' ); ?></h3>
		<div class="bonus-points">
			<div class="bonus-system">
				<?php
				$current_user_id = get_current_user_id();
				$available_bonus = $this->get_user_bonus_count( $current_user_id );

				echo sprintf(
					'<p class="bonuses-available">%s : <b>%d</b></p>',
					esc_html( __( 'Bonuses available', 'bp' ) ),
					esc_html( $available_bonus )
				);

				?>
			</div>
		</div>
		<?php
	}

	/**
	 * Change order in my account.
	 *
	 * @param array $items Menu items.
	 *
	 * @return array
	 */
	public function change_order_in_my_account( array $items ): array {
		$new_order = [
			'dashboard'       => $items['dashboard'],
			'bonus-point'     => $items['bonus-point'],
			'orders'          => $items['orders'],
			'downloads'       => $items['downloads'],
			'edit-address'    => $items['edit-address'],
			'edit-account'    => $items['edit-account'],
			'customer-logout' => $items['customer-logout'],
		];

		return $new_order;
	}

}

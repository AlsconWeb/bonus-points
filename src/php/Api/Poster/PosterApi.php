<?php
/**
 * Integration Poster CRM.
 *
 * @package bp/wordpress-plugin.
 */

namespace BP\Plugin\Api\Poster;

use BP\Plugin\Helpers;
use BP\Plugin\WooCommerce\BonusSystems;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * PosterApi class file.
 */
class PosterApi {

	/**
	 * Sync user in poster action name.
	 */
	public const BP_SYNC_ACTION_NAME = 'bp_sync_action_name';

	/**
	 * Http Client.
	 *
	 * @var Client
	 */
	public Client $client;

	/**
	 * Poster CRM  personal Integration.
	 *
	 * @var string
	 */
	public string $poster_token;

	/**
	 * PosterApi construct.
	 */
	public function __construct() {
		$this->client = new Client();
		$this->init();
		if ( empty( $this->poster_token ) ) {
			$this->poster_token = get_option( '_pb_poster_token' );
		}
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_ajax_' . self::BP_SYNC_ACTION_NAME, [ $this, 'sync_user' ] );
		add_action( 'wp_ajax_nopriv_' . self::BP_SYNC_ACTION_NAME, [ $this, 'sync_user' ] );

		add_action( 'init', [ $this, 'register_cron_sync_poster_in_wp' ] );
		add_action( 'init', [ $this, 'register_cron_sync_wp_in_bonus_poster' ] );
	}

	/**
	 * Create user in poster.
	 *
	 * @param array $user_data User data from poster.
	 *
	 * @return void
	 */
	public function create_user_in_poster( $user_data ): void {

		$url = 'https://joinposter.com/api/clients.createClient?token=' . $this->poster_token;

		try {
			$data = json_decode(
				$this->client->request( 'post', $url, $user_data )
					->getBody()
					->getContents()
			);
		} catch ( GuzzleException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		} catch ( \JsonException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		}
	}

	/**
	 * Add cron sync user in wp.
	 *
	 * @return void
	 */
	public function register_cron_sync_poster_in_wp(): void {
		if ( ! wp_next_scheduled( 'sync_in_wp' ) ) {
			wp_schedule_event( strtotime( 'today 10:00am' ), 'daily', 'sync_in_wp' );
		}

		add_action( 'sync_in_wp', [ $this, 'cron_sync_in_wp' ] );
	}

	/**
	 * Handler cron sync in wp.
	 *
	 * @return void
	 */
	public function cron_sync_in_wp(): void {
		$_POST['nonceSync'] = wp_create_nonce( self::BP_SYNC_ACTION_NAME );
		$this->sync_user();
	}

	/**
	 * Synchronization user ajax handler.
	 *
	 * @return void
	 */
	public function sync_user(): void {
		$nonce = ! empty( $_POST['nonceSync'] ) ? filter_var( wp_unslash( $_POST['nonceSync'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::BP_SYNC_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'bp' ) ] );
		}

		try {
			$groups_array = $this->get_poster_groups();
		} catch ( \Exception $e ) {
			error_log( $e->getMessage() );
		}

		foreach ( $groups_array as $key => $value ) {
			$group_id   = $key;
			$user_count = $value;

			$pages_number = ceil( $user_count / 100 );

			if ( 0 === (int) $pages_number ) {
				$pages_number = 1;
			}

			$users = [];

			for ( $i = 0; $i < $pages_number; $i ++ ) {
				$page = $i * 100;
				try {
					$users = array_merge( $users, $this->get_users( $group_id, $page ) );
				} catch ( GuzzleException $e ) {
					//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
					error_log( $e->getMessage() );
				}
			}

			if ( ! empty( $users ) ) {
				foreach ( $users as $user ) {
					$this->create_customers( $user );
				}
			}

		}
		wp_send_json_success( [ 'message' => __( 'All done', 'bp' ) ] );
	}

	/**
	 * Get groups ID|Name.
	 *
	 * @param bool $select Return the array for the Select.
	 *
	 * @return array
	 * @throws GuzzleException
	 */
	public function get_poster_groups( bool $select = false ): array {

		if ( ! $select ) {
			$data = get_transient( 'bp_groups_array' );
		} else {
			$data = get_transient( 'bp_groups_select' );
		}


		if ( ! empty( $data ) ) {
			return $data;
		}

		$this->poster_token = get_option( '_pb_poster_token' );

		if ( empty( $this->poster_token ) ) {
			return [];
		}

		$url = 'https://joinposter.com/api/clients.getGroups?token=' . $this->poster_token;

		$data = [];
		try {
			$response  = $this->client->request( 'GET', $url );
			$body      = $response->getBody()->getContents();
			$json_data = json_decode( $body );

			if ( ! empty( $json_data ) ) {
				foreach ( $json_data->response as $datum ) {
					$group_id    = 0;
					$group_count = 0;
					$group_name  = '';
					foreach ( $datum as $key_x => $value ) {
						if ( 'client_groups_id' === $key_x ) {
							$group_id = $value;
						}

						if ( 'count_groups_clients' === $key_x ) {
							$group_count = $value;
						}

						if ( 'client_groups_name' === $key_x ) {
							$group_name = $value;
						}
					}

					if ( ! $select ) {
						$data[ $group_id ] = $group_count;
					} else {
						$data[ $group_id ] = $group_name;
					}
				}

				if ( ! $select ) {
					set_transient( 'bp_groups_array', $data, HOUR_IN_SECONDS );
				} else {
					set_transient( 'bp_groups_select', $data, HOUR_IN_SECONDS );
				}
			}
		} catch ( GuzzleException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		}

		return $data;
	}

	/**
	 * Get Users.
	 *
	 * @param int $group_id Group ID.
	 * @param int $page     Page offset.
	 *
	 * @return array
	 */
	public function get_users( int $group_id, int $page = 0 ): array {

		$url = 'https://joinposter.com/api/clients.getClients?token=' . $this->poster_token . '&group_id=' . $group_id . '&num=100&offset=' . $page;

		try {
			$data = json_decode(
				$this->client->request( 'GET', $url )
					->getBody()
					->getContents()
			);
		} catch ( GuzzleException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		} catch ( \JsonException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return [];
	}

	/**
	 * Create user.
	 *
	 * @param object $user Answer of api Poster getClients.
	 *
	 * @return bool
	 */
	public function create_customers( object $user ): bool {
		if ( empty( $user->email ) ) {
			return false;
		}

		$wp_user_id = get_user_by( 'email', $user->email )->ID;

		if ( Helpers::isset_user_in_bonus_sys( $user->client_id ) && ! empty( $wp_user_id ) ) {
			Helpers::update_user_bonus( $wp_user_id, (int) $user->bonus / 100, $user->client_id );

			return true;
		}

		if ( ! Helpers::isset_user_in_bonus_sys( $user->client_id ) && ! empty( $wp_user_id ) ) {
			Helpers::link_users_by_id( $wp_user_id, $user->client_id, (int) $user->bonus / 100 );

			return true;
		}

		$password  = wp_generate_password( 10, false, false );
		$user_data = [
			'user_login'      => $user->email,
			'user_pass'       => $password,
			'user_email'      => $user->email,
			'first_name'      => $user->firstname,
			'last_name'       => $user->lastname,
			'admin_color'     => 'fresh',
			'user_registered' => current_time( 'Y-m-d H:i:s' ),
			'role'            => 'customer',
		];

		$user_id = wp_insert_user( $user_data );
		if ( is_wp_error( $user_id ) ) {
			return false;
		}

		$this->send_access_to_user( $user->email, $password );
		$bonus_system = new BonusSystems();
		$bonus_system->set_user_bonus( 0, $user_id, $user->client_id );

		return true;
	}

	/**
	 * Send credentials.
	 *
	 * @param string $email User email.
	 * @param string $pswd  User password.
	 *
	 * @return void
	 */
	private function send_access_to_user( string $email, string $pswd ): void {
		$to      = $email;
		$subject = __( 'Login details on site: ', 'bp' ) . get_bloginfo( 'name' );
		$message = __( 'Welcome to our website! Your login:', 'bp' ) . $email . __( ' Your password: ', 'bp' ) . $pswd;
		wp_mail( $to, $subject, $message );
	}

	/**
	 * Add cron sync in wp in poster bonus.
	 *
	 * @return void
	 */
	public function register_cron_sync_wp_in_bonus_poster(): void {
		if ( ! wp_next_scheduled( 'sync_in_poster' ) ) {
			wp_schedule_event( strtotime( 'today 12:00am' ), 'daily', 'sync_in_poster' );
		}

		add_action( 'sync_in_poster', [ $this, 'cron_sync_in_poster' ] );
	}

	/**
	 * Handler cron sync in poster.
	 *
	 * @return void
	 */
	public function cron_sync_in_poster(): void {
		$users = Helpers::get_all_users();

		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
				$poster_user_data = $this->get_poster_user_by_id( $user->poster_id );

				if ( empty( $poster_user_data ) ) {
					continue;
				}

				$user_bonus_in_poster = $poster_user_data->bonus;
				$user_bonus_in_wp     = $user->bonus_count * 100;
				$user_bonus_diff      = 0;

				if ( $user_bonus_in_poster > $user_bonus_in_wp ) {
					$user_bonus_diff = $user_bonus_in_poster - $user_bonus_in_wp;
					$total_bonus     = ( $user_bonus_in_wp + $user_bonus_diff ) / 100;
					$bonus_system    = new BonusSystems();
					$bonus_system->set_user_bonus( $total_bonus, $user->user_id, $user->poster_id );
				}

				if ( $user_bonus_in_wp > $user_bonus_in_poster ) {
					$this->set_bonus_in_poster( $user->poster_id, $user_bonus_in_wp / 100 );
				}
			}
		}
	}

	/**
	 * Get poster user by id.
	 *
	 * @param int $poster_user_id Poster user id.
	 *
	 * @return mixed|object
	 */
	private function get_poster_user_by_id( int $poster_user_id ) {
		$url = 'https://joinposter.com/api/clients.getClient?token='
		       . $this->poster_token
		       . '&client_id=' . $poster_user_id;

		try {
			$data = json_decode(
				$this->client->request( 'get', $url )
					->getBody()
					->getContents()
			);
		} catch ( GuzzleException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		} catch ( \JsonException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		}

		if ( ! empty( $data->response ) ) {
			return $data->response[0];
		}


		return (object) [];
	}

	/**
	 * Set user bonus in poster.
	 *
	 * @param int $poster_user Poster client id.
	 * @param int $bonus       User bonus.
	 *
	 * @return void
	 */
	public function set_bonus_in_poster( int $poster_user, int $bonus ): void {

		if ( 0 === $bonus ) {
			return;
		}

		$user_data['form_params'] = [
			'client_id' => $poster_user,
			'bonus'     => $bonus,
		];

		$url = 'https://joinposter.com/api/clients.updateClient?token=' . $this->poster_token;

		try {
			$data = json_decode(
				$this->client->request( 'post', $url, $user_data )
					->getBody()
					->getContents()
			);
		} catch ( GuzzleException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		} catch ( \JsonException $e ) {
			//phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( $e->getMessage() );
		}
	}
}
